# Building a virtual machine to host as many secure (HTTPS) websites as you want

## Overview
1. Create a Virtual Machine instance
1. Software installs
1. Create website paths and files
1. Create VirtualHost files
1. Assign a static IP address
1. Install HTTPS certificates
1. Renew HTTPS certificates

## Note
Whenever you add a new website to host, you just need to follow these steps:

    * Create website paths and files
    * Create VirtualHost files


# Create a Virtual Machine instance

Google Cloud Platform > (top-left menu) Compute Engine > VM instance

Click "Enable" if you're prompted to enable the "Compute Engine API"

Create a VM instance

    * region = (somewhere close to you)
    * machine type = micro f1
    * boot disk = Ubuntu 20.04 LTS (don't choose "Minimum" build - that's for servers that won't be logged into by users)
    * firewall: allow HTTP and HTTPS

# Software installs

Once the VM is created, click "SSH" to launch a Secure Shell to the new VM

    * Google Cloud Platform > (top-left menu) Compute Engine > VM instances
    * SSH (for the given VM instance) > Open in browser window

Update and upgrade all software

    sudo apt update
    sudo apt upgrade

Install the Apache server

    sudo apt install apache2

# Create website paths and files

Create the folders for each website's files

    sudo mkdir -p /var/www/MY_FIRST_SITE.com/html
    sudo mkdir -p /var/www/MY_SECOND_SITE.net/html
    sudo mkdir -p /var/www/MY_THIRD_SITE.org/html
    ...

Create test HTML files for each website

    cd /var/www/MY_FIRST_SITE.com/html
    sudo nano index.html

```
<html>
    <head>
        <title>MY_SITE</title>
    </head>
    <body>
        Welcome to MY_SITE!
    </body>
</html>
```

    (Control-X > Y (Yes) > save as index.html)

    (remember to do these steps for every website!)


# Create the Virtual Hosts files for HTTP (not HTTPS)

NOTE: this is a good tutorial https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts
    
Add *.conf file for every website

    cd /etc/apache2/sites-available
    sudo nano MY_FIRST_SITE.com.conf
    sudo nano MY_SECOND_SITE.net.conf
    sudo nano MY_THIRD_SITE.org.conf
    ...

Then for each of the *.conf files, use this as your template:

```
<VirtualHost *:80>
    ServerAdmin admin@MY_SITE.COM
    ServerName MY_SITE.COM
    ServerAlias www.MY_SITE.COM
    DocumentRoot /var/www/MY_SITE.COM/html

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Example:

    sudo nano susan.net.conf

```
<VirtualHost *:80>
    ServerAdmin admin@susan.net
    ServerName susan.net
    ServerAlias www.susan.net
    DocumentRoot /var/www/susan.net/html

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Load the configuration file into the Apache server & restart Apache

    sudo a2ensite MY_FIRST_SITE.com.conf
    sudo a2ensite MY_SECOND_SITE.net.conf
    sudo a2ensite MY_THIRD_SITE.org.conf
    ...

    sudo a2dissite 000-default.conf
    
    sudo service apache2 reload


# Assign a static IP address

Change the IP from "ephemeral" to "static"

    * Google Cloud Platform > (top-left menu) Networking > VPC Network > External IP Addresses
    * change "ephemeral" to "static"

Configure the DNS records for all domains

    * Go to domains.google.com > My domains
    * for each domain, go to DNS, and edit the "A" record to point to the new static IP
    * for each domain, go to DNS, and add a "CNAME" record for "www" to point to the root domain

    * example:      
                    @       A       1.2.3.4   
                    www     CNAME   mysite.net                    (Google Domains will add a final '.', so it'll read 'punchbug.net.')


Check that the web server works on HTTP

    Open a browser to http://MY_SITE.COM
    Open a browser to http://www.MY_SITE.COM


# Install HTTPS certificates

Install "Let's Encrypt"

    sudo snap install --classic certbot

Install an HTTPS certificate for every website and domain:

    sudo certbot --apache --webroot-path /var/www/MY_FIRST_SITE.com/html -d MY_FIRST_SITE.com -d www.MY_FIRST_SITE.com
    
Example:

    sudo certbot --apache --webroot-path /var/www/susan.net/html -d susan.net -d www.susan.net
    

# Renew HTTPS certificates

To renew your certificates:

    sudo certbot renew

